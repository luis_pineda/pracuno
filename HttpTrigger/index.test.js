// Constantes para usar de forma posterior
const functions = require('./index');
const context = require('../testing/Context');

// Funcion para probar
test('HTTP trigger', async() => {
    // Lo que necesito enviar al function
    const request = {
        query : {name : 'wuicho'}
    };

    await functions(context, request);
    expect(context.res.body).toEqual('Hello, wuicho');
    expect(context.log.mock.calls.length).toBe(1);
});